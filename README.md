# thinkokay

#### 介绍
ThinkOkay采用ThinkPHP5.0提供最基础的RABC功能，同时在此基础上添加了代码生成功能。开发者可以根据简单的设置生成相应的CRUD控制器和视图文件，减少重复的工作.

#### 软件架构
软件架构说明
1.PHP框架采用ThinkPHP5.0；
2.数据库：MySQL（MariaDB）；
3.前端采用Bootstrap4、jQuery；
4.整体采用单页面的形式加载后台页面；

#### 安装教程

1. 新建数据库和表，并修改配置数据库配置文件
2. 管理员默认账号：administrator/123456


#### 使用说明

1. 主要功能就是后台框架基础的RABC
2. 在RABC的基础上添加了代码生成功能，可以通过简单的设置生成需要的CRUD功能，并且已经加入了权限控制。

#### 系统预览
整体图片
![Image text](https://gitee.com/djxfire/thinkokay/attach_files/download?i=220872&u=http%3A%2F%2Ffiles.git.oschina.net%2Fgroup1%2FM00%2F07%2F1C%2FPaAvDFycvMaAYPwRAAHHFi1wfjE112.png%3Ftoken%3D23e4c2242c84149e691bd789ef5508d4%26ts%3D1553775963%26attname%3Dpage1.png)
代码生成
![Image text](https://gitee.com/djxfire/thinkokay/attach_files/download?i=220873&u=http%3A%2F%2Ffiles.git.oschina.net%2Fgroup1%2FM00%2F07%2F1C%2FPaAvDFycvOSAEvq5AADnlGpckW4777.png%3Ftoken%3D793b618def099419fbfab9dc31bcf8f3%26ts%3D1553775963%26attname%3Dpage2.png)
#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)