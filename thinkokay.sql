/*


 Source Server Type    : MySQL
 Source Host           : localhost:3306
 Source Schema         : thinkokay

*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_auth
-- ----------------------------
DROP TABLE IF EXISTS `sys_auth`;
CREATE TABLE `sys_auth`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `remark` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `adduser` int(11) NOT NULL DEFAULT 0,
  `addtime` int(10) NOT NULL DEFAULT 0,
  `modify` int(11) NOT NULL,
  `modifytime` int(10) NOT NULL DEFAULT 0,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1:有效，2：禁用',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_auth
-- ----------------------------
INSERT INTO `sys_auth` VALUES (1, '超级管理员', '超级管理员拥有所有权限', 0, 0, 0, 0, 1);
INSERT INTO `sys_auth` VALUES (3, '测试权限', '', 1, 2147483647, 0, 0, 1);
INSERT INTO `sys_auth` VALUES (4, '测试权限2', '', 1, 2147483647, 1, 2147483647, 2);
INSERT INTO `sys_auth` VALUES (5, '测试权限3', '测试权限3的备注', 0, 0, 0, 0, 2);

-- ----------------------------
-- Table structure for sys_auth_record
-- ----------------------------
DROP TABLE IF EXISTS `sys_auth_record`;
CREATE TABLE `sys_auth_record`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `auth_id` int(11) NOT NULL DEFAULT 0,
  `rule` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `menu_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 52 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_auth_record
-- ----------------------------
INSERT INTO `sys_auth_record` VALUES (1, 1, '#', 1);
INSERT INTO `sys_auth_record` VALUES (2, 1, '', 2);
INSERT INTO `sys_auth_record` VALUES (3, 1, '', 3);
INSERT INTO `sys_auth_record` VALUES (4, 1, '', 4);
INSERT INTO `sys_auth_record` VALUES (5, 1, '', 5);
INSERT INTO `sys_auth_record` VALUES (6, 1, '', 6);
INSERT INTO `sys_auth_record` VALUES (7, 1, '', 7);
INSERT INTO `sys_auth_record` VALUES (8, 1, '', 8);
INSERT INTO `sys_auth_record` VALUES (9, 1, '', 14);
INSERT INTO `sys_auth_record` VALUES (10, 1, '', 12);
INSERT INTO `sys_auth_record` VALUES (11, 1, '', 13);
INSERT INTO `sys_auth_record` VALUES (12, 1, '', 15);
INSERT INTO `sys_auth_record` VALUES (13, 1, '', 16);
INSERT INTO `sys_auth_record` VALUES (14, 1, '', 17);
INSERT INTO `sys_auth_record` VALUES (15, 1, '', 18);
INSERT INTO `sys_auth_record` VALUES (27, 1, '', 25);
INSERT INTO `sys_auth_record` VALUES (28, 1, '', 26);
INSERT INTO `sys_auth_record` VALUES (29, 1, '', 27);
INSERT INTO `sys_auth_record` VALUES (30, 1, '', 28);
INSERT INTO `sys_auth_record` VALUES (31, 1, '', 29);
INSERT INTO `sys_auth_record` VALUES (32, 1, '', 30);
INSERT INTO `sys_auth_record` VALUES (33, 1, '', 31);
INSERT INTO `sys_auth_record` VALUES (34, 1, '', 32);
INSERT INTO `sys_auth_record` VALUES (35, 1, '', 33);
INSERT INTO `sys_auth_record` VALUES (36, 1, '', 34);
INSERT INTO `sys_auth_record` VALUES (37, 1, '', 35);
INSERT INTO `sys_auth_record` VALUES (38, 1, '', 36);
INSERT INTO `sys_auth_record` VALUES (39, 1, '', 37);
INSERT INTO `sys_auth_record` VALUES (40, 1, '', 38);
INSERT INTO `sys_auth_record` VALUES (41, 1, '', 39);
INSERT INTO `sys_auth_record` VALUES (42, 1, '', 40);
INSERT INTO `sys_auth_record` VALUES (43, 1, '', 41);
INSERT INTO `sys_auth_record` VALUES (44, 1, '', 42);
INSERT INTO `sys_auth_record` VALUES (45, 1, '', 43);
INSERT INTO `sys_auth_record` VALUES (46, 1, '', 44);
INSERT INTO `sys_auth_record` VALUES (47, 1, '', 45);
INSERT INTO `sys_auth_record` VALUES (49, 1, '', 46);
INSERT INTO `sys_auth_record` VALUES (50, 1, '', 47);
INSERT INTO `sys_auth_record` VALUES (51, 1, '', 48);

-- ----------------------------
-- Table structure for sys_code
-- ----------------------------
DROP TABLE IF EXISTS `sys_code`;
CREATE TABLE `sys_code`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `table_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `controller_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `addtime` datetime(0) NULL DEFAULT NULL,
  `adduser` int(11) NULL DEFAULT NULL,
  `package` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_code
-- ----------------------------
INSERT INTO `sys_code` VALUES (9, 'bs_project', 'Project', 'design/project/index', '2019-03-28 18:30:44', 1, 'design');

-- ----------------------------
-- Table structure for sys_dictionary
-- ----------------------------
DROP TABLE IF EXISTS `sys_dictionary`;
CREATE TABLE `sys_dictionary`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `key` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_dictionary
-- ----------------------------
INSERT INTO `sys_dictionary` VALUES (2, '启用禁止', 'YES_OR_NO', '');

-- ----------------------------
-- Table structure for sys_dictionary_set
-- ----------------------------
DROP TABLE IF EXISTS `sys_dictionary_set`;
CREATE TABLE `sys_dictionary_set`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `dict` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `dict_key` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `dict_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_dictionary_set
-- ----------------------------
INSERT INTO `sys_dictionary_set` VALUES (13, 'YES_OR_NO', '1', '启用');
INSERT INTO `sys_dictionary_set` VALUES (14, 'YES_OR_NO', '2', '禁止');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT 0,
  `type` tinyint(2) NOT NULL DEFAULT 1 COMMENT '1：菜单，2：按钮，3：接口',
  `status` tinyint(2) UNSIGNED NOT NULL DEFAULT 1 COMMENT '1:有效，2：禁用',
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `icon` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `remark` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `rule` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `data` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `sort` smallint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 51 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, 0, 1, 1, '系统', 'fa fa-gears', '系统设置', '#', '', 9999);
INSERT INTO `sys_menu` VALUES (2, 46, 1, 1, '管理员', 'fa fa-group', '管理员管理', 'admin/user/index', '', 1);
INSERT INTO `sys_menu` VALUES (3, 46, 1, 1, '菜单管理', 'fa fa-bars', '菜单管理', 'admin/menu/index', '', 0);
INSERT INTO `sys_menu` VALUES (4, 46, 1, 1, '权限管理', 'fa fa-shield', '权限管理', 'admin/auth/index', '', 2);
INSERT INTO `sys_menu` VALUES (5, 0, 1, 1, '首页', 'fa fa-fighter-jet', '首页', 'admin/index/main', '', 0);
INSERT INTO `sys_menu` VALUES (7, 3, 2, 1, '编辑菜单', '#', '编辑菜单', 'admin/menu/edit', '', 0);
INSERT INTO `sys_menu` VALUES (8, 3, 2, 1, '新增菜单', 'fa fa-plus', '新增菜单', 'admin/menu/add', '', 1);
INSERT INTO `sys_menu` VALUES (12, 3, 2, 1, '删除菜单', '#', '', 'admin/menu/del', '', 2);
INSERT INTO `sys_menu` VALUES (15, 4, 2, 1, '新增权限', '#', '新增权限', 'admin/auth/add', '', 2);
INSERT INTO `sys_menu` VALUES (16, 4, 2, 1, '编辑权限', '', '编辑权限', 'admin/auth/edit', '', 3);
INSERT INTO `sys_menu` VALUES (17, 4, 2, 1, '删除权限', '', '删除权限', 'admin/auth/del', '', 4);
INSERT INTO `sys_menu` VALUES (18, 4, 2, 1, '分配权限', '', '分配权限', 'admin/auth/auth', '', 5);
INSERT INTO `sys_menu` VALUES (26, 2, 2, 1, '新增用户', '', '', 'admin/user/add', '', 1);
INSERT INTO `sys_menu` VALUES (27, 2, 2, 1, '编辑用户', '', '', 'admin/user/edit', '', 2);
INSERT INTO `sys_menu` VALUES (28, 2, 2, 1, '分配角色', '', '', 'admin/user/auth', '', 3);
INSERT INTO `sys_menu` VALUES (29, 2, 2, 1, '启用禁用', '', '', 'admin/user/forbid', '', 4);
INSERT INTO `sys_menu` VALUES (42, 41, 1, 1, '请假管理', '', '', '/app/index', 'p=ceshiliucheng&n=shangjizhuguanshenhe', 0);
INSERT INTO `sys_menu` VALUES (43, 42, 2, 1, '新增请假单', '#', '', '/app/forms', 'p=ceshiliucheng&n=shangjizhuguanshenhe&b=xinzeng', 1);
INSERT INTO `sys_menu` VALUES (45, 44, 1, 1, '外发单', 'fa fa-child', '', '/app/index', 'p=teshugongyi&n=waifashenqing', 1);
INSERT INTO `sys_menu` VALUES (46, 1, 1, 1, '系统管理', 'fa fa-gears', '', '#', '', 0);
INSERT INTO `sys_menu` VALUES (47, 1, 1, 1, '代码生成', 'fa fa-code', '', 'admin/code/index', '', 2);
INSERT INTO `sys_menu` VALUES (48, 4, 2, 1, '启用禁用', '', '', 'admin/auth/forbid', '', 3);
INSERT INTO `sys_menu` VALUES (49, 3, 2, 1, '启用禁用', '', '', 'admin/menu/forbid', '', 0);
INSERT INTO `sys_menu` VALUES (50, 1, 1, 1, '字典管理', 'fa fa-book', '', 'admin/dictionary/index', '', 3);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `valid` tinyint(3) NOT NULL DEFAULT 1 COMMENT '1:有效，2：禁用',
  `email` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `adduser` int(11) NOT NULL DEFAULT 0,
  `modifyuser` int(11) NOT NULL DEFAULT 0,
  `addtime` int(10) NOT NULL,
  `modifytime` int(10) NOT NULL,
  `mobile` varchar(15) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'administrator', 'e10adc3949ba59abbe56e057f20f883e', 1, '516385822@qq.com', 0, 0, 0, 0, '');
INSERT INTO `sys_user` VALUES (2, 'test111', '!a123456', 1, 'janwoolx45@163.com', 1, 0, 2147483647, 0, '');

-- ----------------------------
-- Table structure for sys_user_auth
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_auth`;
CREATE TABLE `sys_user_auth`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `auth_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_user_auth
-- ----------------------------
INSERT INTO `sys_user_auth` VALUES (1, 1, 1);
INSERT INTO `sys_user_auth` VALUES (7, 3, 2);
INSERT INTO `sys_user_auth` VALUES (8, 4, 2);

SET FOREIGN_KEY_CHECKS = 1;
