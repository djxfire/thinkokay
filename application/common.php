<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件

function hasAuth($rule) {
    return \service\AuthService::checkAuthNode($rule);
}
/**
* 下划线转驼峰
*/
function camelize($uncamelized_words,$separator='_'){
    $uncamelized_words = $separator. str_replace($separator, " ", strtolower($uncamelized_words));
    return ltrim(str_replace(" ", "", ucwords($uncamelized_words)), $separator );
}/**
* 驼峰命名转下划线命名
*/
function uncamelize($camelCaps,$separator='_'){
    return strtolower(preg_replace('/([a-z])([A-Z])/', "$1" . $separator . "$2", $camelCaps));
}

function get_select_option($descript) {
    $optiones = explode(":", $descript);
    $html = '';
    if($optiones[0] == "DC") {
        $dcs = \think\Db::name("SysDictionarySet")->where("dict", 'YES_OR_NO')->fields('dict_key as key, dict_value as value')->select();
        foreach ($dcs as $dc) {
            $html .= "<option value = '{$dc['key']}'>{$dc['value']}</option>";
        }
    }else {
        $dbs = \think\Db::name(ccamelize($optiones[1]))->fields("{$optiones[2]} as key, {$optiones[3]} as value")->select();
        foreach ($dbs as $dc) {
            $html .= "<option value = '{$dc['key']}'>{$dc['value']}</option>";
        }
    }
    return $html;

}