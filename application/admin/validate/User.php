<?php
/**
 * Created by PhpStorm.
 * User: LENOVO
 * Date: 2019/3/18
 * Time: 21:25
 */

namespace app\admin\validate;


use think\Validate;

class User extends Validate{
    protected $rule = [
        "username|用户名"    => 'require',
        "password|密码"      => 'require'
    ];
    protected $message = [
        "username.require"  => "用户名称不能为空",
        "password.require"  => "密码不能为空"
    ];
}