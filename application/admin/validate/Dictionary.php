<?php
/**
 * Created by PhpStorm.
 * User: LENOVO
 * Date: 2019/3/28
 * Time: 17:32
 */

namespace app\admin\validate;


use think\Db;
use think\Validate;

class Dictionary extends Validate{
    protected $rule = [
        "name|名称"    => 'require',
        "key|键值"      => 'require'
    ];
    protected $message = [
        "name.require"  => "字典名称不能为空",
        "key.require"  => "字典键值不能为空",
    ];
    protected  $scene =[
        'add' => ['key' => 'require|unique']
    ];
    protected function unique($value, $rule, $data, $field){
        $has = Db::name("SysDictionary")->where("key", $value)->count();
        return $has > 0?"已存在该字典键值":true;
    }
}