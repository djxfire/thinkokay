<?php
/**
 * Created by PhpStorm.
 * User: LENOVO
 * Date: 2019/3/20
 * Time: 23:35
 */

namespace app\admin\controller;


use common\Ret;
use controller\AuthBasic;
use service\DataService;
use service\QueryService;
use think\Db;
use think\Exception;
use think\Log;
use think\Paginator;

class User extends AuthBasic
{
    public $title = "用户管理";
    public $table = "SysUser";


    protected function _index_where_filter(&$query,  $where){
        $filter = [
            "like" => ['username', 'email', 'mobile'],
            "="    => ['valid']
        ];
        QueryService::decorateQuery($query, $where, $filter);
    }

    public function index() {
        return $this->_list($this->table, input(''));
    }

    protected function _add_form_filter(&$vo) {
        if($this->request->isPost()) {
            $hasUser = Db::name($this->table)->where('username', $vo['username'])->count();
            if($hasUser > 0) {
                return '用户名已经存在';
            }
            $vo['password'] = md5('123456');
        }
    }
    protected function _edit_form_filter(&$vo) {
        if($this->request->isPost()) {
            $hasUser = Db::name($this->table)->where('username', $vo['username'])->count();
            if($hasUser > 0) {
                return '用户名已经存在';
            }
        }
    }
    public function add() {
        return $this->_form($this->table, 'form');
    }

    public function edit() {
        return $this->_form($this->table, 'form');
    }
    protected function _password_form_filter(&$vo) {
        if($this->request->isPost()) {
            if($vo['password'] !== $vo['repassword']) {
                return '两次密码不一致';
            }
            $vo['password'] = md5($vo['password']);
            unset($vo['repassword']);
        }
    }

    public function password() {
        return $this->_form($this->table, 'pass');
    }
    public function auth() {
        if(!$this->request->isPost()) {
            $id = input('id');
            $auth_ids = Db::name("SysUserAuth")->where('user_id', $id)->column("auth_id");
            $auths = Db::name("SysAuth")->select();
            $user = Db::name("SysUser")->where("id", $id)->find();
            $this->assign("user", $user);
            $this->assign("auth_ids", $auth_ids);
            $this->assign("auths", $auths);
            return $this->fetch();
        }
        $auths = input('auths/a', []);
        $id = input('id');
        Db::startTrans();
        try{
            Db::name("SysUserAuth")->where('user_id', $id)->delete();
            $user_auths = [];
            foreach($auths as $auth) {
                array_push($user_auths, [
                    "user_id"   => $id,
                    "auth_id"   => $auth
                ]);
            }
            Db::name("SysUserAuth")->insertAll($user_auths);
            Db::commit();
            return $this->ret->setCode(Ret::$_RET_RELOAD)->setMsg("保存成功")->toJson();
        }catch(Exception $e) {
            Db::rollback();
            return $this->ret->setCode(Ret::$_RET_ERROR)->setMsg('系统错误，错误：'.$e->getMessage())->toJson();
        }

    }
    public function del() {
        if (DataService::update($this->table)) {
            return $this->ret->setCode(Ret::$_RET_RELOAD)->setMsg('删除成功')->toJson();
        }
        return $this->ret->setCode(Ret::$_RET_ERROR)->setMsg('删除失败，请重试')->toJson();
    }
    public function forbid() {
        if (DataService::update($this->table)) {
            return $this->ret->setCode(Ret::$_RET_RELOAD)->setMsg('操作成功')->toJson();
        }
        return $this->ret->setCode(Ret::$_RET_ERROR)->setMsg('操作失败，请重试')->toJson();
    }
}