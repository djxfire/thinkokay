<?php
/**
 * Created by PhpStorm.
 * User: LENOVO
 * Date: 2019/3/18
 * Time: 23:05
 */

namespace app\admin\controller;


use controller\AuthBasic;
use service\AuthService;

class Index extends AuthBasic{
    public $title = "首页";
    public function index() {

        AuthService::applyAuthNode();
        $nodes = (array)session("user.nodes");

        $sorted_nodes = AuthService::sortNode($nodes);
        return $this->fetch('', ['title' => '系统管理', 'nodes' => $sorted_nodes]);
    }
    public function main() {
        $this->assign('title', $this->title);
        return $this->fetch();
    }

}