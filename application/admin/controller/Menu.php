<?php
/**
 * Created by PhpStorm.
 * User: LENOVO
 * Date: 2019/3/19
 * Time: 21:56
 */

namespace app\admin\controller;


use common\Ret;
use controller\AuthBasic;
use service\AuthService;
use service\DataService;
use service\QueryService;
use think\Db;


class Menu extends AuthBasic {
    public $title = "菜单管理";
    public $table = 'SysMenu';
    protected function _index_where_filter(&$query,  $where){
        $filter = [
            "like" => ['name', 'rule', 'remark'],
            "="    => ['type', 'status']
        ];
        QueryService::decorateQuery($query, $where, $filter);
    }
    protected function _index_data_filter(&$data) {
        $data['list'] = AuthService::sortNode($data['list']);
        return true;
    }
    protected function _form_filter(&$vo){
        $nodes = AuthService::getNodes();
        $sorted_nodes = AuthService::sortNode($nodes);
        $this->assign('nodes', $sorted_nodes);

    }
    public function index() {
        $query = Db::name($this->table)->order('sort asc');
        return $this->_list($query, input(''),false);
    }

    public function add() {
        return $this->_form($this->table, 'form');
    }
    public function edit() {
        return $this->_form($this->table, 'form');
    }
    public function del() {
        if (DataService::update($this->table)) {
            return $this->ret->setCode(Ret::$_RET_RELOAD)->setMsg('删除成功')->toJson();
        }
        return $this->ret->setCode(Ret::$_RET_ERROR)->setMsg('删除失败，请重试')->toJson();
    }
    public function forbid() {
        if (DataService::update($this->table)) {
            return $this->ret->setCode(Ret::$_RET_RELOAD)->setMsg('操作成功')->toJson();
        }
        return $this->ret->setCode(Ret::$_RET_ERROR)->setMsg('操作失败，请重试')->toJson();
    }
}