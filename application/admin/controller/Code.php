<?php
/**
 * Created by PhpStorm.
 * User: LENOVO
 * Date: 2019/3/26
 * Time: 0:57
 */

namespace app\admin\controller;


use common\Ret;
use controller\AuthBasic;
use service\CodeService;
use service\DataService;
use think\Db;

class Code extends AuthBasic{
    public $table = "SysCode";
    public $title = "代码管理";
    public function index() {
        return $this->_list($this->table);
    }

    public function add() {
        if(!$this->request->isPost()) {
            $has_generators = Db::name($this->table)->column("table_name");
            $has_generators_exp = implode("','", $has_generators);
            $tables = Db::query("select table_name from information_schema.tables 
                                        where table_schema = '".config('database.database')."' 
                                        and table_name not like 'sys%'
                                        and table_name not in ('".$has_generators_exp."')" );
            return $this->fetch('form',["tables" => $tables]);
        }
        $fields = input("fields/a");
        $table_name = input("table_name");
        $package = input("package");
        $controller_name = input("controller_name");
        if(empty($package)) {
            $package = "admin";
        }
        $controller_cam = uncamelize($controller_name);
        if(!file_exists("../application/{$package}")){
            mkdir("../application/${package}", 0777, true);
        }
        if(!file_exists("../application/{$package}/controller")) {
            mkdir("../application/${package}/controller", 0777, true);
        }
        if(!file_exists("../application/{$package}/view/$controller_cam")) {
            mkdir("../application/${package}/view/$controller_cam", 0777, true);
        }
        $res = CodeService::generate_controller($package, $controller_name, camelize($table_name), $fields);

        if($res['code'] === 0) {
            $res = CodeService::generator_view_of_index($package, $controller_name, camelize($table_name), $fields);
            if($res['code'] === 0) {
                $res = CodeService::generator_view_of_form($package, $controller_name, camelize($table_name), $fields);
                if($res['code'] === 0) {

                    Db::name("SysCode")->insert([
                        "table_name"    => $table_name,
                        "controller_name" => $controller_name,
                        "package"   => $package,
                        "url"       => "{$package}/{$controller_cam}/index",
                        "addtime"   => date("Y-m-d H:i:s"),
                        "adduser"   => session("user.id")
                    ]);
                    return $this->ret->setCode(Ret::$_RET_RELOAD)->setMsg("新建成功")->toJson();
                }else {
                    return $this->ret->setCode(Ret::$_RET_ERROR)->setMsg($res['msg'])->toJson();
                }
            }else {
                return $this->ret->setCode(Ret::$_RET_ERROR)->setMsg($res['msg'])->toJson();
            }

        }else {
            return $this->ret->setCode(Ret::$_RET_ERROR)->setMsg($res['msg'])->toJson();
        }
    }

    public function get_fields() {
        $table_name = input('table_name');
        $fields = Db::query("select COLUMN_NAME,DATA_TYPE from information_schema.COLUMNS where table_name = '{$table_name}' and table_schema = '".config('database.database')."'");
        return $this->ret->setCode(Ret::$_RET_SUCCESS)->setData("fields", $fields)->toJson();
    }


    public function del() {
        if (DataService::update($this->table)) {
            return $this->ret->setCode(Ret::$_RET_RELOAD)->setMsg('删除成功')->toJson();
        }
        return $this->ret->setCode(Ret::$_RET_ERROR)->setMsg('删除失败，请重试')->toJson();
    }
}